-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 17, 2023 at 08:27 AM
-- Server version: 8.0.30
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `unidagon_ilkes`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `user_id` int NOT NULL,
  `created_at` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('mahasiswa', 14, 1686722376),
('mahasiswa', 15, 1689411901),
('theCreator', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `type` smallint NOT NULL,
  `description` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `rule_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, 'ADMIN MASTER', NULL, NULL, 1616483344, 1616483344),
('dosen', 1, 'Dosen dan Umum dapat melakukan peminjaman logistik, instrumen dan ruangan', NULL, NULL, NULL, NULL),
('ka_laboran', 1, 'Laboran dapat melihat semua insight dari semua data sistem yang diinput, memperbaharui stok barang laboratorium dan dapat membuat laporan mingguan untuk setiap kegiatan dan laporan penggunaan bahan alat di laboratorium.', NULL, NULL, NULL, NULL),
('laboran', 1, 'Laboran dapat melihat semua insight dari semua data sistem yang diinput dan memperbaharui stok barang laboratorium.', NULL, NULL, NULL, NULL),
('mahasiswa', 1, 'student', NULL, NULL, NULL, NULL),
('theCreator', 1, 'Super Admin', NULL, NULL, 1616483344, 1616483344);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `child` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('theCreator', 'admin'),
('admin', 'mahasiswa'),
('dosen', 'mahasiswa');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('isAuthor', 0x4f3a32353a226170705c726261635c72756c65735c417574686f7252756c65223a333a7b733a343a226e616d65223b733a383a226973417574686f72223b733a393a22637265617465644174223b693a313636333538373834383b733a393a22757064617465644174223b693a313636333538373834383b7d, 1663587848, 1663587848);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8mb3_unicode_ci NOT NULL,
  `apply_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1668528329),
('m221111_144133_create_auth_item_child_table', 1668528337),
('m221115_150802_create_kelompok_table', 1668528337),
('m221115_152804_create_ruangan_table', 1668528337),
('m221115_152815_create_logistik_table', 1668528337),
('m221115_152830_create_peminjaman_table', 1668528337),
('m221115_152859_create_praktikum_table', 1668528337),
('m221115_153748_create_jenis_table', 1668528337),
('m221115_153757_create_satuan_table', 1668528337),
('m221218_033448_create_tempat_table', 1672150109),
('m221218_034129_create_dosen_table', 1672150109),
('m221218_034906_create_judul_table', 1672150109),
('m221218_035403_create_data_prodi_table', 1672150109),
('m221218_035902_create_ruangan_table', 1672150110),
('m221218_043010_create_barang_pinjam_table', 1672150110),
('m221220_075133_create_peminjaman_table', 1672150110),
('m221220_142130_create_barang_pinjam_table', 1672150110),
('m221220_170503_create_barang_pinjam_table', 1672150110),
('m221221_112538_add_foreign_key', 1672150111),
('m221221_121907_drop_column', 1672150111),
('m221221_123917_add_column_peminjaan', 1672150111),
('m221221_174958_create_barang_pinjam_table', 1672150112),
('m221222_021759_drop_columns', 1672150112),
('m221222_035941_table_barang_pinjam', 1672150112),
('m221227_135754_create_nama_gedung_table', 1672150112),
('m221227_141100_addColumn_no', 1672150436),
('m221227_141851_dropColumn_no', 1672150768),
('m221227_144900_create_bahan_table', 1672152662),
('m230103_030611_create_bahan_coba_table', 1672715298),
('m230103_043530_create_alat_table', 1672807076),
('m230103_044558_create_ruangan_table', 1672895330),
('m230103_163805_add_foreign_key', 1672895674),
('m230103_170136_create_gedung_table', 1672906252),
('m230104_040850_create_satuanr_table', 1672805509),
('m230105_161411_create_pegawai_table', 1672935354),
('m230105_162320_add_pegawai_colomn', 1672935849),
('m230105_173900_add_pegawai_colomn', 1672940401),
('m230105_175402_add_pegawai_colomn', 1672941334),
('m230105_175640_add_pegawai_colomn', 1672943699),
('m230105_184531_add_user_column', 1672944519),
('m230105_190518_create_biaya_penelitian_table', 1672945552),
('m230105_191306_create_responsible_table', 1673031334),
('m230106_190232_create_gedung_table', 1673031785),
('m230106_191349_create_anggota_table', 1673032552),
('m230106_191927_add_peminjaman_column', 1673083345),
('m230107_092305_add_peminjaman_column', 1673083474),
('m230108_032128_create_jenis_kegiatan_table', 1673148178),
('m230613_034327_add_column', 1686627912),
('m230613_035252_Relations', 1686629650),
('m230613_035704_Relations', 1686629650),
('m230613_120419_add_column', 1686658231),
('m230613_121337_add_foignkey', 1686658667),
('m230613_123839_add_foignkey', 1686660428),
('m230613_124816_add_foignkey', 1686661428),
('m230613_130542_add_foignkey', 1686661657),
('m230715_065922_full_migration', 1689495441);

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` char(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `expire` int DEFAULT NULL,
  `data` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `expire`, `data`) VALUES
('sorfhc0g7f2vjns79t6uoj6js6', 1692091071, 0x5f5f666c6173687c613a303a7b7d5f5f69647c693a313b5f5f617574684b65797c733a33323a224875636a4f636665753359566d38685351594958566e38736d48745954467850223b5f5f6578706972657c693a313639323630393437313b),
('td81e1ts34aprbr81hdpbbu454', 1691129423, 0x5f5f666c6173687c613a303a7b7d5f5f69647c693a313b5f5f617574684b65797c733a33323a224875636a4f636665753359566d38685351594958566e38736d48745954467850223b5f5f6578706972657c693a313639313634373832333b);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `username` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `uuid` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `account_activation_token` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `otp` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb3_unicode_ci NOT NULL,
  `access_role` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `auditor_id` int DEFAULT NULL,
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `nim` int DEFAULT NULL,
  `kontak` int DEFAULT NULL,
  `prodi_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `uuid`, `nama`, `email`, `auth_key`, `password_hash`, `password_reset_token`, `account_activation_token`, `otp`, `status`, `access_role`, `auditor_id`, `created_at`, `updated_at`, `nim`, `kontak`, `prodi_id`) VALUES
(1, 'superadmin', '591deb6d-f126-4208-857e-bdb5de8dda95', 'Super Admin', 'pptik@unida.gontor.ac.id', 'HucjOcfeu3YVm8hSQYIXVn8smHtYTFxP', '$2a$13$mRUypj2h/dQjFA4nLHRb4.4/0R6aJbachfZxGtbwTksIGg.JutrR2', NULL, NULL, NULL, '10', 'theCreator', NULL, 1522377073, 1522377073, NULL, NULL, NULL),
(14, 'nashehannafii', NULL, NULL, 'nasheh@gmail.com', 'VxHCs3JX3PBN1OuZZNlsnHLslHrTwTqm', '$2y$13$GO/HQO9E0dYpo1atsqkfquSECWIl07RrfT4v0ARu3hG3DXOGUYL6a', NULL, NULL, NULL, '10', 'mahasiswa', NULL, 1686722376, 1686722376, NULL, NULL, NULL),
(15, 'test', NULL, NULL, 'test@test.com', 'fo_x3TnFfcVfWN4kWqiUuzNYHsLHHpY7', '$2y$13$O1iQDm7eJ9UAUeBc//Y0XOzDysOuBkzvm8EvNYvNODsanH8GkvuDi', NULL, NULL, NULL, '10', 'mahasiswa', NULL, 1689411901, 1689411901, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auditor_id` (`auditor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`),
  ADD CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `fk_auth_assignment_item_name` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_auth_assignment_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
